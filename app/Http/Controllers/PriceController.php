<?php

namespace App\Http\Controllers;

use App\Models\Prices;
use Illuminate\Http\Request;

class PriceController extends Controller
{
    public function update(Request $request)
    {
        if ($request[0]['key']!=="key") 
        {
            return response()->json(['message' => 'False key']);
        }
        
        else
        {
            $data = $request[0]['data'];
            $prices = $data[0]['prices'];
            for ($i = 0; $i < count($data); $i++) {
                foreach ($prices as $key => $v) {
                    Prices::updateOrInsert(
                    ['product_id' => $data[$i]['product_id'],
                    'region_id' => $key,
                    ], 
                    ['price_purchase' => $prices[$key]['price_purchase'],
                    'price_selling' => $prices[$key]['price_selling'],
                    'price_discount' => $prices[$key]['price_discount']
                    ]
                );}
            }
            
            return response()->json([
                'message' => 'Prices updated',
                'data' => Prices::all()
            ]);
        }
    }
}
