<?php

namespace Database\Seeders;

use App\Models\Region;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class RegionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('regions')->insert(
            [
                ['name' => 'Тюменская область'],
                ['name' => 'Московская область'],
                ['name' => 'Ленинградская область'],
                ['name' => 'Свердловская область'],
                ['name' => 'Архангельская область'],
                ['name' => 'Астраханская область'],
                ['name' => 'Оренбургская область'],
                ['name' => 'Новосибирская область'],
                ['name' => 'Мурманская область'],
                ['name' => 'Ярославская область'],
                ['name' => 'Воронежская область'],
                ['name' => 'Тульская область'],
                ['name' => 'Смоленская область'],
                ['name' => 'Саратовская область'],
                ['name' => 'Псковская область'],
                ['name' => 'Вологодская область'],
                ['name' => 'Тамбовская область'],
                ['name' => 'Томская область'],
                ['name' => 'Ульяновская область'],
                ['name' => 'Магаданская область'],
            ]
        );
    }
}
